from django.urls import path, include, re_path

from ..controllers.authController import AuthController as authController
from ..controllers.commonController import CommonController as commonController
from ..controllers.userController import UserController as userController
from ..controllers.roleController import RoleController as roleController
from ...utils.envUtils import routeName
from ..controllers.views import NotFoundView


loginPatterns = [
    path('welcome', authController.welcome, name='welcome'),
    path('login', authController.signIn, name='login'),
]

commmonPatterns = [
    path('common/encrypt', commonController.encrypt, name='encrypt'),
    path('common/decrypt', commonController.decrypt, name='decrypt'),
]

userPatterns = [
    path('user', userController.getAllDataUser, name='user-list'),
    path('user/getDataById/<str:idMasterUsers>', userController.getDataUserById, name='user-detail'),
    path('user/getAllDataUserByParam', userController.getAllDataUserByParam, name='user-by-param'),
    path('user/store', userController.store, name='user-store'),
    path('user/update', userController.update, name='user-update'),
    path('user/updateIsActive', userController.updateIsActive, name='user-isActived'),
]

rolePatterns = [
    path('role', roleController.getAllDataRole, name='role-list'),
    path('role/getDataById/<str:idMasterRoles>', roleController.getDataRoleById, name='role-detail'),
    path('role/store', roleController.store, name='role-store'),
    path('role/update', roleController.update, name='role-update'),
    path('role/updateIsActive', roleController.updateIsActive, name='role-isActived'),
]

urlpatterns = [
    path(routeName, include(loginPatterns)),
    path(routeName, include(commmonPatterns)),
    path(routeName, include(userPatterns)),
    path(routeName, include(rolePatterns)),

    re_path(r'^.*', NotFoundView.as_view(), name='not_found'),
]