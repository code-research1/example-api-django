# exception_handler.py
from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException

from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant

class InternalServerError(APIException):
    status_code = 500
    default_detail = 'Internal server error'
    default_code = 'internal_server_error'

def customExceptionHandler(exc, context):
    response = exception_handler(exc, context)

    if not response and isinstance(exc, InternalServerError):
        response = commonUtils.GeneralResponse(500, constant.ERROR, {'detail': 'Internal server error'})
    
    return response
