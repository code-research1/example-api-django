from django.db import transaction
from django.db.models import Q


from ..models.entity.userEntity import UserEntity

class UserRepository:
    @staticmethod
    def getAllDataUsers():
        return UserEntity.objects.all().order_by('fullname')
    
    @staticmethod
    def getDataUserById(userId: str):
        try:
            result = UserEntity.objects.get(idMasterUsers=userId)
            return result
        except UserEntity.DoesNotExist:
            raise

    @staticmethod
    def getAllDataUserByParam(key: str, value: str):
        try:
            result = UserEntity.objects.get(email=value)

            return result
        except UserEntity.DoesNotExist:
            raise    
    
    @staticmethod
    def store(paramStore):
        try:
            with transaction.atomic():
                paramStore.save()
        except Exception as e:
            transaction.rollback()
            raise e    
        
    @staticmethod
    def update(existingUser, idMasterRoles, fullname, username, isGender
               , address, hpNumber, email, updateBy):
        try:
            existingUser.idMasterRoles = idMasterRoles
            existingUser.fullname = fullname
            existingUser.username = username
            existingUser.isGender = isGender
            existingUser.address  = address
            existingUser.hpNumber = hpNumber
            existingUser.email    = email
            existingUser.updatedBy = updateBy

            with transaction.atomic():
                existingUser.save()

            return existingUser
        except Exception as e:
            transaction.rollback()
            raise e
        
    @staticmethod
    def isActived(existingUser, isActive, updateBy):
        try:
            existingUser.isActive = isActive
            existingUser.updatedBy = updateBy

            with transaction.atomic():
                existingUser.save()

            return existingUser
        except Exception as e:
            transaction.rollback()
            raise e    