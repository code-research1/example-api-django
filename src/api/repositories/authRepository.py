from django.db import transaction

from ..models.entity.userEntity import UserEntity
from ...utils.commonUtils import CommonUtils as commonUtils
from ...constants import applicationConstant as constants

class AuthRepository:
    @staticmethod
    def checkLogin(username: str, password: str):
        try:
            result = UserEntity.objects.get(username=username)
            decryptPassword = commonUtils.DecryptAes256Sha256(password, constants.PASSWORD)
            if commonUtils.VerifyPassword(decryptPassword, result.password):
                return result
            else:
                result = None
                return result
        except UserEntity.DoesNotExist:
            raise     