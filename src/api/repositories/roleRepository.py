from django.db import transaction

from ..models.entity.roleEntity import RoleEntity

class RoleRepository:
    @staticmethod
    def getAllDataRoles():
        return RoleEntity.objects.all().order_by('roleName')
    
    @staticmethod
    def getDataRoleById(roleId: str):
        try:
            result = RoleEntity.objects.get(idMasterRoles=roleId)
            return result
        except RoleEntity.DoesNotExist:
            raise

    @staticmethod
    def store(paramStore):
        try:
            with transaction.atomic():
                paramStore.save()
        except Exception as e:
            transaction.rollback()
            raise e    
        
    @staticmethod
    def update(existingRole, roleName, description, updateBy):
        try:
            existingRole.roleName = roleName
            existingRole.description = description
            existingRole.updatedBy = updateBy

            with transaction.atomic():
                existingRole.save()

            return existingRole
        except Exception as e:
            transaction.rollback()
            raise e
        
    @staticmethod
    def isActived(existingRole, isActive, updateBy):
        try:
            existingRole.isActive = isActive
            existingRole.updatedBy = updateBy

            with transaction.atomic():
                existingRole.save()

            return existingRole
        except Exception as e:
            transaction.rollback()
            raise e          