import uuid
import jwt
import sys
from functools import wraps
from datetime import datetime, timedelta

from ...utils.commonUtils import CommonUtils as commonUtils
from ...constants import infoConstant as constants
from ...utils.envUtils import apiKeyEncode, apiKey, signatureKeyEncode

import logging
logger = logging.getLogger(__name__)
from ...utils.loggerUtils import CustomJSONFormatter as customLog

class AuthMiddleware:

    @staticmethod
    def GenerateToken():
        payload = {
            "authorized": True,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(hours=3),
            "iss": apiKeyEncode,
            "jti": str(uuid.uuid4())
        }

        token = jwt.encode(payload, apiKey, algorithm='HS256') 
        return token

    @staticmethod
    def ValidateToken(f):
        @wraps(f)
        def decorated(request, *args, **kwargs):
            try:
                token = None 
                authHeader = request.headers.get('Authorization')
                apiKeyHeader = request.headers.get('Api-Key')
                signatureHeader = request.headers.get('Signature')
                signatureTimeHeader = request.headers.get('Signature-Time')

                apiKeyEncode2 = apiKeyEncode.encode()
                signatureEncode2 = signatureKeyEncode.encode() 

                if apiKeyHeader != apiKeyEncode2 and apiKeyHeader is None:
                    customLog.CreateLog(logger, constants.ERROR, constants.ERR_MESSAGE_HEADER_API, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.ERR_MESSAGE_HEADER_API)
                    return response
                    
                if signatureHeader != signatureEncode2 and signatureHeader is None:
                    customLog.CreateLog(logger, constants.ERROR, constants.ERR_MESSAGE_HEADER_SIGNATURE, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.ERR_MESSAGE_HEADER_SIGNATURE)
                    return response
                    
                if signatureTimeHeader is None:
                    customLog.CreateLog(logger, constants.ERROR, constants.ERR_MESSAGE_HEADER_SIGNATURE_TIME, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.ERR_MESSAGE_HEADER_SIGNATURE_TIME)
                    return response

                parts = authHeader.split()

                if len(parts) == 2 and parts[0].lower() == 'bearer':
                    token = parts[1]
                else:
                    customLog.CreateLog(logger, constants.ERROR, constants.TOKEN_INVALID, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.TOKEN_INVALID)
                    return response

                if not token:
                    customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(500, constants.ERROR, constants.TOKEN_PROVIDED)
                    return response

                try:
                    data = jwt.decode(token, apiKey, algorithms=['HS256'])
                    return f(request, data, *args, **kwargs)
                except jwt.ExpiredSignatureError:
                    customLog.CreateLog(logger, constants.ERROR, constants.PROVIDED_TOKEN_EXPIRED, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.PROVIDED_TOKEN_EXPIRED)
                    return response
                except jwt.InvalidTokenError:
                    customLog.CreateLog(logger, constants.ERROR, constants.TOKEN_INVALID, 401, sys._getframe().f_lineno, None)
                    response = commonUtils.GeneralResponse(401, constants.ERROR, constants.TOKEN_INVALID)
                    return response
            except Exception as e:
                customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, {'message': constants.INTERNAL_SERVER_ERROR})
                response = commonUtils.GeneralResponse(500, constants.ERROR, {'message': constants.INTERNAL_SERVER_ERROR})
                return response

        return decorated
