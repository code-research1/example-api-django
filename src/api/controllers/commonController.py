import json
import base64
import os
from django.http import JsonResponse
from rest_framework.decorators import api_view

from ...constants import applicationConstant as constants
from ...utils.commonUtils import CommonUtils as commonUtils

class CommonController():

    @api_view(['POST'])
    def encrypt(request):
        requestModel = json.loads(request.body)
        if requestModel.get('keyType') == constants.DATA:
            password = constants.KEY_AES.encode('utf-8')
        else:
            password = constants.KEY_PASS_AES.encode('utf-8')
        salt = os.urandom(16)

        key = commonUtils.GenerateKey(password, salt)
        encryptedData = commonUtils.EncryptData(key, requestModel.get('data').encode('utf-8'))

        ciphertext = salt + encryptedData

        ciphertextBase64 = base64.b64encode(ciphertext).decode('utf-8')

        return JsonResponse({'ciphertext': ciphertextBase64})
    
    @api_view(['POST'])
    def decrypt(request):        
        requestModel = json.loads(request.body)
        keyType = requestModel.get('keyType')
        if keyType == constants.DATA:
            password = constants.KEY_AES.encode('utf-8')
        else:
            password = constants.KEY_PASS_AES.encode('utf-8')

        data = base64.b64decode(requestModel.get('encryptedData').encode('utf-8'))

        salt = data[:16]
        ciphertext = data[16:]

        key = commonUtils.GenerateKey(password, salt)
        decryptedData = commonUtils.DecryptData(key, ciphertext)

        return JsonResponse({'plaintext': decryptedData.decode('utf-8')})
    
    