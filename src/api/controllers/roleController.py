import json
import uuid
import sys
from django.db import transaction
from rest_framework.decorators import api_view

from ..repositories.roleRepository import RoleRepository as roleRepository
from ..models.serializers.roleSerializers import RoleSerializer as roleSerializer
from ...constants import infoConstant as constants
from ...utils.commonUtils import CommonUtils as commonUtils
from ..models.entity.roleEntity import RoleEntity
from ..middlewares.authMiddleware import AuthMiddleware as authMiddleware

import logging
logger = logging.getLogger(__name__)
from ...utils.loggerUtils import CustomJSONFormatter as customLog



class RoleController():

    @api_view(['GET'])
    @authMiddleware.ValidateToken
    def getAllDataRole(request, data):
        try:
            getAllDataRoles = roleRepository.getAllDataRoles()
            listDataRoles = roleSerializer(getAllDataRoles, many=True)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, listDataRoles.data)
            return response
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, {'message': str(e)})
            response = commonUtils.GeneralResponse(500, constants.ERROR, {'message': constants.INTERNAL_SERVER_ERROR})
            return response
    
    @api_view(['GET'])
    @authMiddleware.ValidateToken
    def getDataRoleById(request, data, idMasterRoles):        
        try:
            existingRole = roleRepository.getDataRoleById(idMasterRoles)
            
            listDataRoles = roleSerializer(existingRole)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, listDataRoles.data)
            return response
        except RoleEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
            return response
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def store(request, data):
        try:
            requestModel = json.loads(request.body)
            paramStore = RoleEntity(
                idMasterRoles   = str(uuid.uuid4()),
                roleName        = requestModel.get('roleName'),
                description     = requestModel.get('description'),
                createdBy       = requestModel.get('createdBy'),
                isActive        = constants.ACTIVED,
            )
            with transaction.atomic():
                roleRepository.store(paramStore)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_ADD)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def update(request, data):
        try:
            requestModel = json.loads(request.body)
            existingRole = roleRepository.getDataRoleById(requestModel.get('idMasterRoles'))
            updatedRole = roleRepository.update(
                existingRole,
                requestModel.get('roleName'),
                requestModel.get('description'),
                requestModel.get('updatedBy')
            )
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_UPDATE)
               
        except RoleEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
            return response
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))
        
        return response
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def updateIsActive(request, data):
        try:
            requestModel = json.loads(request.body)
            existingRole = roleRepository.getDataRoleById(requestModel.get('idMasterRoles'))

            updatedRole = roleRepository.isActived(
                existingRole,
                requestModel.get('isActive'),
                requestModel.get('updatedBy')
            )
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_DELETE)
                
        except RoleEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    
    