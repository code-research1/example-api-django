import json
import sys
from django.http import JsonResponse
from rest_framework.decorators import api_view

from ...utils.envUtils import appName
from ..repositories.authRepository import AuthRepository as authRepository
from ...constants import infoConstant as constants
from ...utils.commonUtils import CommonUtils as commonUtils
from ..models.entity.userEntity import UserEntity
from ..middlewares.authMiddleware import AuthMiddleware as authMiddlware

import logging
logger = logging.getLogger(__name__)
from ...utils.loggerUtils import CustomJSONFormatter as customLog

class AuthController():

    @api_view(['POST'])
    def signIn(request):
        try:
            requestModel = json.loads(request.body)
            existingUser = authRepository.checkLogin(requestModel.get('username'), requestModel.get('password'))
            
            if existingUser is None:
                customLog.CreateLog(logger, constants.SUCCESS, constants.GENERAL_ERROR, 404, sys._getframe().f_lineno, constants.ERR_PASSWORD_NOT_MATCH)
                response = commonUtils.GeneralResponse(404, constants.GENERAL_ERROR, constants.ERR_PASSWORD_NOT_MATCH)
                return response
            
            accessToken = authMiddlware.GenerateToken()
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, accessToken.decode('utf-8'))
                
        except UserEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    

    @api_view(['GET'])
    def welcome(self):        
        message = f"Welcome to Web Service {appName}"
        data = {'message': message}
        return JsonResponse(data)
    