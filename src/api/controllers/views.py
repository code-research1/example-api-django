# views.py
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


from src.utils.commonUtils import CommonUtils as commonUtils
from src.constants import infoConstant as constant

class NotFoundView(APIView):
    def get(self, request):
        response = commonUtils.GeneralResponse(status.HTTP_404_NOT_FOUND, constant.ERROR, {'detail': 'Not found'})
        return response
