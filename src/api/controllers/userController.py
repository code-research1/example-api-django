import json
import uuid
import sys
from django.db import transaction
from rest_framework.decorators import api_view

from ..repositories.userRepository import UserRepository as userRepository
from ..models.serializers.userSerializer import UserSerializer as userSerializer
from ...constants import infoConstant as constants
from ...utils.commonUtils import CommonUtils as commonUtils
from ..models.entity.userEntity import UserEntity
from ..middlewares.authMiddleware import AuthMiddleware as authMiddleware

import logging
logger = logging.getLogger(__name__)
from ...utils.loggerUtils import CustomJSONFormatter as customLog

class UserController():

    @api_view(['GET'])
    @authMiddleware.ValidateToken
    def getAllDataUser(request, data):
        try:
            getAllDataUsers = userRepository.getAllDataUsers()

            listDataUsers = userSerializer(getAllDataUsers, many=True)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, listDataUsers.data)
            return response
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, {'message': str(e)})
            response = commonUtils.GeneralResponse(500, constants.ERROR, {'message': constants.INTERNAL_SERVER_ERROR})
            return response
    
    @api_view(['GET'])
    @authMiddleware.ValidateToken
    def getDataUserById(request, data, idMasterUsers):        
        try:
            existingUser = userRepository.getDataUserById(idMasterUsers)
            
            listDataRoles = userSerializer(existingUser)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, listDataRoles.data)
            return response
        except UserEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
            return response
        
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def getAllDataUserByParam(request, data):
        try:
            requestModel = json.loads(request.body)
            existingUser = userRepository.getAllDataUserByParam(requestModel.get('key'), requestModel.get('value'))

            listDataRoles = userSerializer(existingUser)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, listDataRoles.data)
            return response
            
        except UserEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)

        return response    
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def store(request, data):
        try:
            requestModel = json.loads(request.body)
            paramStore = UserEntity(
                idMasterUsers   = str(uuid.uuid4()),
                idMasterRoles   = requestModel.get('idMasterRoles'),
                fullname        = requestModel.get('fullname'),
                username        = requestModel.get('username'),
                isGender        = requestModel.get('isGender'),
                address         = requestModel.get('address'),
                hpNumber        = requestModel.get('hpNumber'),
                email           = requestModel.get('email'),
                password        = commonUtils.GeneratePassword(),
                createdBy       = requestModel.get('createdBy'),
                isActive        = constants.ACTIVED,
            )
            
            with transaction.atomic():
                userRepository.store(paramStore)
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_ADD)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def update(request, data):
        try:
            requestModel = json.loads(request.body)
            existingUser = userRepository.getDataUserById(requestModel.get('idMasterUsers'))

            updatedUser = userRepository.update(
                existingUser,
                requestModel.get('idMasterRoles'),
                requestModel.get('fullname'),
                requestModel.get('username'),
                requestModel.get('isGender'),
                requestModel.get('address'),
                requestModel.get('hpNumber'),
                requestModel.get('email'),
                requestModel.get('updatedBy')
            )

            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_UPDATE)
               
        except UserEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    
    @api_view(['POST'])
    @authMiddleware.ValidateToken
    def updateIsActive(request, data):
        try:
            requestModel = json.loads(request.body)
            existingUser = userRepository.getDataUserById(requestModel.get('idMasterUsers'))

            updatedUser = userRepository.isActived(
                existingUser,
                requestModel.get('isActive'),
                requestModel.get('updatedBy')
            )
            response = commonUtils.GeneralResponse(200, constants.SUCCESS, constants.SUCCESSFULLY_DELETE)
                
        except UserEntity.DoesNotExist:
            customLog.CreateLog(logger, constants.SUCCESS, constants.DATA_NOT_FOUND, 404, sys._getframe().f_lineno, None)
            response = commonUtils.GeneralResponse(404, constants.SUCCESS, constants.DATA_NOT_FOUND)
        except Exception as e:
            customLog.CreateLog(logger, constants.ERROR, constants.GENERAL_ERROR, 500, sys._getframe().f_lineno, str(e))
            response = commonUtils.GeneralResponse(500, constants.GENERAL_ERROR, str(e))

        return response
    
    