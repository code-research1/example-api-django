from django.db import models
from django.utils import timezone
from datetime import datetime  
# Create your models here.
class RoleEntity(models.Model):
    idMasterRoles = models.CharField(
        primary_key=True,
        default='',
        editable=False,
        null=False,
        db_column="id_master_roles"
    )
    roleName = models.CharField(
        null=False,
        default='',
        db_column="role_name"
    )
    description = models.CharField(
        null=True,
        default='',
        db_column="description"
    )
    createdBy = models.CharField(
        null=False,
        default='',
        db_column="created_by"
    )
    updatedBy = models.CharField(
        null=True,
        default='',
        db_column="updated_by"
    )
    isActive = models.CharField(
        null=False,
        default='',
        db_column="is_active"
    )
    createdAt = models.DateTimeField(
        default=timezone.now,
        db_column="created_at"
    )  
    updatedAt = models.DateTimeField(
        default=datetime.now,
        db_column="updated_at"
    )  

    class Meta:
        db_table = 'master_roles'
   