from django.db import models
from django.utils import timezone
from datetime import datetime  
# Create your models here.
class UserEntity(models.Model):
    idMasterUsers = models.CharField(
        primary_key=True,
        default='',
        editable=False,
        null=False,
        db_column="id_master_users"
    )
    idMasterRoles = models.CharField(
        default='',
        editable=False,
        null=False,
        db_column="id_master_roles"
    )
    fullname = models.CharField(
        null=False,
        default='',
        db_column="fullname"
    )
    username = models.CharField(
        null=False,
        default='',
        db_column="username"
    )
    isGender = models.CharField(
        null=False,
        default='',
        db_column="is_gender"
    )
    address = models.CharField(
        null=False,
        default='',
        db_column="address"
    )
    hpNumber = models.CharField(
        null=False,
        default='',
        db_column="hp_number"
    )
    dateActivation = models.DateTimeField(
        default=timezone.now,
        db_column="date_activation"
    )
    email = models.CharField(
        null=False,
        default='',
        db_column="email"
    )
    emailVerifiedAt = models.DateTimeField(
        default=timezone.now,
        db_column="email_verified_at"
    )  
    password = models.CharField(
        null=False,
        default='',
        db_column="password"
    )
    urlPhoto = models.CharField(
        null=False,
        default='',
        db_column="url_photo"
    )
    createdBy = models.CharField(
        null=False,
        default='',
        db_column="created_by"
    )
    updatedBy = models.CharField(
        null=True,
        default='',
        db_column="updated_by"
    )
    isActive = models.CharField(
        null=False,
        default='',
        db_column="is_active"
    )
    createdAt = models.DateTimeField(
        default=timezone.now,
        db_column="created_at"
    )  
    updatedAt = models.DateTimeField(
        default=datetime.now,
        db_column="updated_at"
    )  

    class Meta:
        db_table = 'master_users'
   