from rest_framework import serializers 
from ..entity.roleEntity import RoleEntity
 
 
class RoleSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = RoleEntity
        fields = ('idMasterRoles', 'roleName', 'description', 'createdBy', 'updatedBy', 'isActive', 'createdAt', 'updatedAt')
