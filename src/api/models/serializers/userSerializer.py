from rest_framework import serializers 
from ..entity.userEntity import UserEntity
 
 
class UserSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = UserEntity
        fields = ('idMasterUsers', 'idMasterRoles', 'fullname', 'username', 'isGender'
                  , 'address', 'hpNumber', 'dateActivation', 'email' , 'emailVerifiedAt'
                  , 'urlPhoto', 'createdBy', 'updatedBy' , 'isActive'
                  , 'createdAt', 'updatedAt')
