import os
import yaml

from ..constants import applicationConstant as constants
from .commonUtils import CommonUtils as commonUtils

currentDirectory = os.getcwd()
CONFIG_FILE = os.path.join(currentDirectory, 'config.yaml')

def load_config(filePath=CONFIG_FILE):
    with open(filePath, 'r') as configFile:
        return yaml.safe_load(configFile)

# Load the configuration settings
config = load_config()

# Access configuration settings
appName = config.get('appName', '')
serverPort = config.get('serverPort', '')
environment = config.get('environment', '')
releaseMode = config.get('releaseMode', '')


# Access log directory for the current environment
logDirectory = config.get('logDirectory', {}).get(environment, {}).get('path', '')

# Access database settings for the current environment
dbSettings = config.get('database', {}).get(environment, {})

databaseConnection = commonUtils.DecryptAes256Sha256(dbSettings.get('connection', ''), constants.DATA)
databaseDialect = commonUtils.DecryptAes256Sha256(dbSettings.get('dialect', ''), constants.DATA)
databaseUsername = commonUtils.DecryptAes256Sha256(dbSettings.get('username', ''), constants.DATA)
databasePassword = commonUtils.DecryptAes256Sha256(dbSettings.get('password', ''), constants.DATA)
databaseUrl = commonUtils.DecryptAes256Sha256(dbSettings.get('url', ''), constants.DATA)
databasePort = commonUtils.DecryptAes256Sha256(dbSettings.get('port', 0), constants.DATA)
databaseSchema = commonUtils.DecryptAes256Sha256(dbSettings.get('schema', ''), constants.DATA)

# Access key settings for the current environment
keySettings = config.get('key', {}).get(environment, {})

apiKey = keySettings.get('apiKey', '')
apiKeyEncode = keySettings.get('apiKeyEncode', '')
signatureKey = keySettings.get('signatureKey', '')
signatureKeyEncode = keySettings.get('signatureKeyEncode', '')

# Access route names for the current environment
routeName = config.get('route', {}).get(environment, {}).get('name', '')
