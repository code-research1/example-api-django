import logging
import json
import sys
from datetime import datetime, timezone

class CustomJSONFormatter(logging.Formatter):
    def format(self, record):
        logData = {
            "log.level": record.levelname,
            "@timestamp": datetime.now(timezone.utc).astimezone().isoformat(),
            "message": record.getMessage(),
            "title": record.title,
            "code": record.code,
            "endpoint": record.name,
            "linenumber": record.linenumber,
            "datas": record.datas,
        }
        return json.dumps(logData)

    def CreateLog(logger, title: str, message: str, code: int, linenumber: str, datas: any):
        logger.error(message, extra={
            'title': title,
            'code': code,
            'linenumber': linenumber,
            'datas': datas
        })  