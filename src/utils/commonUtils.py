import time
import bcrypt
import os
import base64

from django.http import JsonResponse
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes

# Additional imports for cryptography
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

from ..constants import applicationConstant as constants

class CommonUtils:

    @staticmethod
    def GetCurrentDateTime():
        return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    
    @staticmethod
    def GeneralResponse(responseCode: int, responseDescription: str, responseDatas: list):
        responseData  = {
            'responseCode': responseCode,
            'responseDescription': responseDescription,
            'responseTime': CommonUtils.GetCurrentDateTime(),
            'responseDatas': responseDatas
        }

        response = JsonResponse(responseData, status = responseCode)
        return response
    
    @staticmethod
    def GeneratePassword():
        password = 'p@ssw0rd'
        bytes = password.encode('utf-8') 
        salt = bcrypt.gensalt() 
        hash = bcrypt.hashpw(bytes, salt)  
        return hash
    
    @staticmethod
    def HashPassword(password: str):
        bytes = password.encode('utf-8') 
        salt = bcrypt.gensalt() 
        hash = bcrypt.hashpw(bytes, salt)  
        return hash
    
    @staticmethod
    def VerifyPassword(providedPassword: str, hashedPassword: str):
        providedPasswordBytes = providedPassword.encode('utf-8') 
        return bcrypt.checkpw(providedPasswordBytes, hashedPassword) 

    @staticmethod
    def GenerateKey(password, salt):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            iterations=100000,
            salt=salt,
            length=32,
        )
        key = kdf.derive(password)
        return key
    
    @staticmethod
    def EncryptData(key, data):
        iv = os.urandom(16)
        cipher = Cipher(algorithms.AES(key), modes.GCM(iv), backend=default_backend())
        encryptor = cipher.encryptor()
        ciphertext = encryptor.update(data) + encryptor.finalize()
        return (iv + encryptor.tag + ciphertext)

    @staticmethod
    def DecryptData(key, data):
        iv = data[:16]
        tag = data[16:32]
        ciphertext = data[32:]
        cipher = Cipher(algorithms.AES(key), modes.GCM(iv, tag), backend=default_backend())
        decryptor = cipher.decryptor()
        plaintext = decryptor.update(ciphertext) + decryptor.finalize()
        return plaintext
    
    @staticmethod
    def DecryptAes256Sha256(encryptedData, keyType):
        
        if keyType == constants.DATA:
            password = constants.KEY_AES.encode('utf-8')
        else:
            password = constants.KEY_PASS_AES.encode('utf-8')
            
        data = base64.b64decode(encryptedData.encode('utf-8'))
        
        # Extract the salt from the ciphertext
        salt = data[:16]
        ciphertext = data[16:]

        key = CommonUtils.GenerateKey(password, salt)
        decryptedData = CommonUtils.DecryptData(key, ciphertext)

        return decryptedData.decode('utf-8')    
    
    
    
    
